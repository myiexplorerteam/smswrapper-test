<?php namespace MyIExplorerTeam\SMSWrapper;

/**
 * laravel service provider
 */

use Illuminate\Support\ServiceProvider;

class SMSWrapperProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/sms-wrapper.php' => config_path('sms-wrapper.php'),
        ]);
        $this->loadViewsFrom(__DIR__.'/views', 'sms-wrapper');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('myiexplorerteam-sms-wrappper', function () {
            return new SMSWrapper('');
        });
    }
}
