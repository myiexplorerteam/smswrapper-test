<?php

namespace MyIExplorerTeam\SMSWrapper\Classes;

use MyIExplorerTeam\SMSWrapper\Classes\Provider\Provider;

class Response
{
    protected $provider;
    protected $return;

    /**
     * Response constructor.
     * @param Provider $provider
     * @param string $return
     */
    public function __construct(Provider $provider, $return)
    {
        $this->provider = $provider;
        $this->return = $return;
    }

    /**
     * @return Provider
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @return string
     */
    public function getReturn()
    {
        return $this->return;
    }
}
