<?php

namespace MyIExplorerTeam\SMSWrapper\Classes\Provider;

abstract class Provider
{
    protected $httpEnable = false;

    abstract public function send($message, $recipients, $from = null);
    abstract public function getName();
    abstract public function getEndPoint();
    abstract protected function getParameter($message, $recipient, $from = null);

    public function __construct(array $settings)
    {
        $this->httpEnable = $settings['http_enable'];
    }

    protected function sendSingleRecipient($message, $recipient, $from = null)
    {
        // parse recipient
        $recipient = $this->parseRecipient($recipient);

        // construct parameter
        $parameter = $this->getParameter($message, $recipient, $from);

        // http get
        return $this->httpGet($this->getEndPoint(), $parameter);
    }

    protected function parseRecipient($recipient)
    {
        // remove -
        $recipient = str_replace('-', '', $recipient);

        // remove space
        $recipient = str_replace(' ', '', $recipient);

        return $recipient;
    }

    protected function httpGet($endPoint, array $parameter)
    {
        // construct $url
        $url = $endPoint . '?' . http_build_query($parameter);

        // curl
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // http call only if enabled
        if ($this->httpEnable) {
            return curl_exec($ch);
        } else {
            return $url;
        }
    }
}
