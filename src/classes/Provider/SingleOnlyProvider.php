<?php

namespace MyIExplorerTeam\SMSWrapper\Classes\Provider;

abstract class SingleOnlyProvider extends Provider
{
    public function send($message, $recipients, $from = null)
    {
        $return = '';

        if (is_array($recipients)) {
            $returns = [];
            foreach ($recipients as $recipient) {
                $returns[] = $this->sendSingleRecipient($message, $recipient, $from);
            }
            $return = implode(';', $returns);
        } else {
            $return = $this->sendSingleRecipient($message, $recipients, $from);
        }

        return $return;
    }
}
