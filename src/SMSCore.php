<?php

/**
 * core for sms
 */

namespace MyIExplorerTeam\SMSWrapper;

use MyIExplorerTeam\SMSWrapper\Classes\Provider\Mexcomm;
use MyIExplorerTeam\SMSWrapper\Classes\Provider\Nexmo;
use MyIExplorerTeam\SMSWrapper\Classes\Provider\Provider;
use MyIExplorerTeam\SMSWrapper\Classes\Provider\Trio;
use MyIExplorerTeam\SMSWrapper\Classes\Response;

class SMSCore
{
    const VERSION = '1.0';

    protected $providers = [];
    protected $settings = [];

    public function __construct($providers, $settings)
    {
        $this->providers = $providers;
        $this->settings = $settings;
    }

    /**
     * @param $message
     * @param $recipients
     * @param null $from
     * @param int $providerIndex
     * @return Response
     */
    public function send($message, $recipients, $from = null, $providerIndex = 0)
    {
        // get provider
        $provider = $this->getProviderToSend($providerIndex);

        $return = $provider->send($message, $recipients, $from);

        return new Response($provider, $return);
    }

    protected function getProviderIndexToSend($providerIndex)
    {
        // check if $providerIndex > providers length
        if ($providerIndex >= count($this->providers)) {
            // get last
            $providerIndex = count($this->providers) - 1;
        }

        return $providerIndex;
    }

    /**
     * @param $providerIndex
     * @return Provider
     */
    protected function getProviderToSend($providerIndex)
    {
        // get provider index to send
        $providerIndex = $this->getProviderIndexToSend($providerIndex);

        // get provider name
        $provider = $this->providers[$providerIndex];

        // get settings
        $settings = $this->getSettings($provider);

        switch ($provider) {
            case Mexcomm::NAME:
                return new Mexcomm($settings);
                break;
            case Nexmo::NAME:
                return new Nexmo($settings);
                break;
            case Trio::NAME:
                return new Trio($settings);
                break;
            default:
                return null;
        }
    }

    /**
     * @param string $provider
     * @return array
     */
    protected function getSettings($provider)
    {
        return $this->settings[$provider];
    }
}
