<?php

return [
    // base on priority
    'providers' => ['mexcomm', 'nexmo'],
    'settings' => [
        'mexcomm' => [
            'user' => 'xxxxx',
            'pass' => 'xxxxx',
            'http_enable' => false,
        ],
        'nexmo' => [
            'api_key' => 'xxxxx',
            'api_secret' => 'xxxxx',
            'http_enable' => false,
        ],
        'trio' => [
            'api_key' => 'xxxxx',
            'http_enable' => false,
        ]
    ],

    // default log setting, can ignore this
    'log' => [
        // logs directory
        'dir' => 'logs',

        // log file name
        'file' => 'sms-wrapper'
    ],
];