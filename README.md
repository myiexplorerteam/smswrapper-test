# SMS-Wrapper ( Laravel 5 Package )

## Installation

composer.json

    "require": [
      {
        "myiexplorerteam/sms-wrapper": "dev-master",
      }
    ],


Just add

    "repositories": [
      {
        "type": "vcs",
        "url":  "git@bitbucket.org:myiexplorerteam/smswrapper.git"
      }
    ],

to your composer.json. Then run the `composer require` command from your terminal:

    composer require myiexplorerteam/sms-wrapper:dev-master

Then in your `config/app.php` add
```php
    MyIExplorerTeam\SMSWrapper\SMSWrapperProvider::class,
```
in the `providers` array.

Then run:

    php artisan vendor:publish

## Config

config/sms-wrapper.php

Supported SMS Service Provider

    'mexcomm', 'nexmo', 'trio'

## TODO

- Check mobile number format valid before send
